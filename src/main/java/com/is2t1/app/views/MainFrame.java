/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.is2t1.app.views;

import com.is2t1.app.controller.MainController;
import javax.swing.JInternalFrame;

/**
 *
 * @author willj
 */
public class MainFrame extends javax.swing.JFrame {
    
    MainController mc;
    
    public MainFrame() {
        initComponents();
        setupController();
    }
    
    private void setupController() {
        mc = new MainController(this);
        this.logoutMenuItem.addActionListener(mc);
        this.contactMenuItem.addActionListener(mc);
    }
    
    public void showChild(JInternalFrame frame,boolean maximizeFrame){
        desktop.add(frame);
        frame.setVisible(true);
        if(maximizeFrame){
            desktop.getDesktopManager().maximizeFrame(frame);            
        }
    }
    
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        desktop = new javax.swing.JDesktopPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        toolMenu = new javax.swing.JMenu();
        logoutMenuItem = new javax.swing.JMenuItem();
        MenuMenu = new javax.swing.JMenu();
        contactMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout desktopLayout = new javax.swing.GroupLayout(desktop);
        desktop.setLayout(desktopLayout);
        desktopLayout.setHorizontalGroup(
            desktopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 614, Short.MAX_VALUE)
        );
        desktopLayout.setVerticalGroup(
            desktopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 525, Short.MAX_VALUE)
        );

        getContentPane().add(desktop, java.awt.BorderLayout.CENTER);

        toolMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/tools.png"))); // NOI18N

        logoutMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/logout.png"))); // NOI18N
        logoutMenuItem.setText("Salir");
        logoutMenuItem.setActionCommand("logout");
        toolMenu.add(logoutMenuItem);

        jMenuBar1.add(toolMenu);

        MenuMenu.setText("Menu");

        contactMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/contact.png"))); // NOI18N
        contactMenuItem.setText("Contacto");
        contactMenuItem.setActionCommand("contact");
        MenuMenu.add(contactMenuItem);

        jMenuBar1.add(MenuMenu);

        setJMenuBar(jMenuBar1);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu MenuMenu;
    private javax.swing.JMenuItem contactMenuItem;
    private javax.swing.JDesktopPane desktop;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem logoutMenuItem;
    private javax.swing.JMenu toolMenu;
    // End of variables declaration//GEN-END:variables

    
}
