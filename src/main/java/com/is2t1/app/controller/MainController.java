/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.is2t1.app.controller;

import com.is2t1.app.views.ContactFrame;
import com.is2t1.app.views.MainFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Sistemas-01
 */
public class MainController implements ActionListener {
    
    MainFrame mainframe;

    public MainController(MainFrame mainframe) {
        this.mainframe = mainframe;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        switch (event.getActionCommand()){
            case "logout":
                System.exit(0);
                break;
            case "contact":
                showContact();
                break;
            default:
                break;
        }
    }

    private void showContact() {
        ContactFrame cf = new ContactFrame();
        mainframe.showChild(cf, true);
        
    }

    
}
